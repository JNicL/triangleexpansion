!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 b0trunc.f90                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for B0^(n)(-p^2,0,m^2) coefficients
program b0trunc
  use b0, only: B0_n_small_z,B0_n_large_z,B0_0
  use triangle_aux, only: prec,target_precision,qp,cone,muUV2
  use test_aux, only: check_real
  
  real(prec) :: cmp

  cmp = -0.19968898382896867839541972887139695748924617093328_prec
  call check_real(B0_n_small_z(1._prec/170._prec**2*cone,5),cmp)

  ! large z expansion
  cmp = 0.00023503264465721768234298201851770548947086730671683_prec
  call check_real(B0_n_large_z(3*cone,4),cmp)
  if (prec .eq. qp) then
    call check_real(B0_n_small_z(3*cone,4),cmp,30)
  else
    call check_real(B0_n_small_z(3*cone,4),cmp,13)
  end if


  cmp = -0.016305315310273884522171011935981053959593479127914_prec
  call check_real(B0_n_large_z(30._prec*cone,1),cmp)
  if (prec .eq. qp) then
    call check_real(B0_n_small_z(30._prec*cone,1),cmp,30)
  else
    call check_real(B0_n_small_z(30._prec*cone,1),cmp,13)
  end if

  cmp = -1.6531989814619501174608709623232577718528085590871_prec*real(1Q-24,kind=prec)
  call check_real(B0_n_large_z(170._prec**2*cone,5),cmp)
  if (prec .eq. qp) then
    call check_real(B0_n_small_z(170._prec**2*cone,5),cmp,7)
  end if

  cmp = -6.6113892381529864853315386063823621656318674869674_prec
  muUV2 = 14.3_prec*cone
  call check_real(B0_0(170._prec**2/3._prec*cone,3._prec*cone),cmp)
end program b0trunc
