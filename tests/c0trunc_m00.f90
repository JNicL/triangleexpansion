!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                               c0trunc_m00.f90                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

program c0trunc_m00
  use c0_m00, only: C0_n_m00_small_z,C0_n_m00_large_z
  use triangle_aux, only: prec,dp,qp,muIR2
  use test_aux, only: check_real

  complex(prec) :: z,p2
  real(prec)    :: cmp

  p2 = 3._prec
  muIR2 = 1._prec
  m2 = 170._prec**2
  z = m2/p2

  cmp = -0.00032078858853627509250698182320934450090265422607698_prec
  call check_real(C0_n_m00_large_z(z,p2,0), cmp)

  cmp = 1.3954855562502737208254594525672808676679968119246_prec*real(1Q-8,kind=prec)
  call check_real(C0_n_m00_large_z(z,p2,1), cmp)

  cmp = -8.6209405078975306299443430844039675397994871489364_prec*real(1Q-13,kind=prec)
  call check_real(C0_n_m00_large_z(z,p2,2), cmp)

  p2 = 100._prec
  muIR2 = 1._prec
  m2 = 1._prec
  z = m2/p2

  cmp = -0.0097071236142332918511611531491841014501196962632698_prec
  call check_real(C0_n_m00_small_z(z,p2,2), cmp)

  cmp = -0.0011269833704663942293475143495830182519970672466820_prec
  call check_real(C0_n_m00_small_z(z,p2,10), cmp)

  p2 = 100._prec
  muIR2 = 1._prec
  z = 1._prec

  cmp = 1.5568337522693009025234688020082110432432100024776_prec*real(1Q-8,kind=prec)
  if (prec .eq. qp) then
    call check_real(C0_n_m00_small_z(z,p2,10), cmp, 30)
    call check_real(C0_n_m00_large_z(z,p2,10), cmp, 30)
  else
    call check_real(C0_n_m00_small_z(z,p2,10), cmp, 10)
    call check_real(C0_n_m00_large_z(z,p2,10), cmp, 13)
  end if

  p2 = 100._prec
  muIR2 = 1._prec
  z = 0.8_prec

  cmp = -0.0013800537529337618820245743610768873623341719743330_prec
  call check_real(C0_n_m00_large_z(z,p2,2), cmp)
  call check_real(C0_n_m00_small_z(z,p2,2), cmp)

  p2 = 100._prec
  muIR2 = 1._prec
  z = 0.5_prec

  cmp = 0.000029737547811022630323555107696050335264723332804866_prec
  if (prec .eq. qp) then
    call check_real(C0_n_m00_small_z(z,p2,7), cmp)
  else
    call check_real(C0_n_m00_small_z(z,p2,7), cmp, 13)
  end if

  p2 = 100._prec
  muIR2 = 1._prec
  z = 0.5_prec

  cmp = 1.1627485918885839511195626014333845644515430073701_prec*real(1Q-7,kind=prec)
  call check_real(C0_n_m00_large_z(z,p2,20), cmp)
  if (prec .eq. qp) then
    call check_real(C0_n_m00_small_z(z,p2,20), cmp, 29)
  end if

  z = 0.4_prec
  p2 = 100._prec
  muIR2 = 22._prec

  cmp = 1.7249153575913028662057540499187365045334085709180_prec*real(1Q-6,kind=prec)
  call check_real(C0_n_m00_large_z(z,p2,20), cmp)
  if (prec .eq. qp) then
    call check_real(C0_n_m00_small_z(z,p2,20), cmp, 30)
  else
    ! for z ~0.5 the small_z expression does not perform well.
    call check_real(C0_n_m00_small_z(z,p2,20), cmp, 13)
  end if

  z = 0.01_prec
  p2 = 10._prec
  muIR2 = 22._prec

  cmp = -0.023926067023502193257318384181655334084342309594830_prec
    
  if (prec .eq. qp) then
    call check_real(C0_n_m00_large_z(z,p2,15), cmp, 28)
  end if
  call check_real(C0_n_m00_small_z(z,p2,15), cmp)

  z = 0.1_prec
  cmp = -0.0073540357467611784595440671639236826161874734076338_prec
  call check_real(C0_n_m00_large_z(z,p2,15), cmp)
  call check_real(C0_n_m00_small_z(z,p2,15), cmp)

end program c0trunc_m00
