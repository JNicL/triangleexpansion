!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                test_aux.f90                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! utils for unittests

module test_aux

  use triangle_aux, only: prec,target_precision,i8,dp,cima
  implicit none

  interface check_real
    module procedure check_real_complexin, check_real_realin
  end interface

  contains

  subroutine check_real_complexin(computed, reference, digits)
    ! compare the computed result against a reference value
    complex(prec), intent(in)        :: computed
    real(prec), intent(in)           :: reference
    integer,    intent(in), optional :: digits
    real(prec)                       :: delta, tp
    if (present(digits)) then
      call check_real_realin(real(computed,kind=prec),reference,digits)
    else
      call check_real_realin(real(computed,kind=prec),reference)
    end if
  end subroutine check_real_complexin

  subroutine check_real_realin(computed, reference, digits)
    ! compare the computed result against a reference value
    real(prec), intent(in)           :: computed, reference
    integer,    intent(in), optional :: digits
    real(prec)                       :: delta, tp

    delta = abs((reference-computed)/computed)

    if (present(digits)) then
      tp = 10._prec**real(-digits+1,kind=prec)
    else
      if (prec .eq. dp) then
        tp = 13
      else
        tp = 32
      end if
      tp = 10._prec**real(-tp+1,kind=prec)
    end if

    if (delta .gt. tp) then
      write(*,*) "digits:",-ceiling(log(delta)/log(10d0))
      write(*,*) "computed: ", computed
      write(*,*) "reference:", reference
      call EXIT(1)
    end if
  end subroutine check_real_realin

  subroutine check_integer(computed, reference)
    ! compare the computed integer against a reference value
    integer(kind=i8), intent(in) :: computed, reference
    integer(kind=i8)             :: delta

    delta = abs(reference-computed)
    if (delta .gt. 0) then
      write(*,*) "computed: ", computed
      write(*,*) "reference:", reference
      call EXIT(1)
    end if
  end subroutine check_integer

  subroutine check_complex(computed, reference, digits)
    ! compare the computed result against a reference value
    complex(prec), intent(in)           :: computed
    complex(prec), intent(in)           :: reference
    integer,       intent(in), optional :: digits
    real(prec)                       :: delta, tp
    if (present(digits)) then
      call check_real_realin(real(computed,prec),real(reference,prec),digits)
      call check_real_realin(real(-cima*computed,prec),real(-cima*reference,prec),digits)
    else
      call check_real_realin(real(computed,prec),real(reference,prec))
      call check_real_realin(real(-cima*computed,prec),real(-cima*reference,prec))
    end if
  end subroutine check_complex

end module test_aux
