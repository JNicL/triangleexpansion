!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                   b0.f90                                    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

program b0
  use triangle_reduction, only: B0d
  use triangle_aux, only: prec,dp,qp,cone,cnul,muUV2
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m2
  real(prec) :: p2,z,d,cmp

  p2 = 3._prec
  z = 1._prec
  m2 = z*p2
  d = 5._prec/100._prec
  muUV2 = 14.3_prec

  cmp = 2.1601505095899905129072832958907227866311369092546_prec
  call check_real(B0d(p2,m2,d,0),cmp)

  cmp = -0.30404755775741545920893578232964139192491148541951_prec
  call check_real(B0d(p2,m2,d,1),cmp)

  cmp = 0.056105233652784627476641924243640799991767604404631_prec
  call check_real(B0d(p2,m2,d,2),cmp)

  p2 = 3._prec
  z = 1._prec
  m2 = z*p2
  d = 5._prec/100._prec
  muUV2 = 1._prec

  cmp = -0.50010902767587108319015626895536091039473640081424_prec
  call check_real(B0d(p2,m2,d,0),cmp)

  p2 = 1._prec
  m2 = 3._prec
  d = 0.1_prec
  muUV2 = 14.3_prec

  cmp = -0.13577354440898978115359788629347066826011641941587_prec
  call check_real(B0d(p2,m2,d,1),cmp)

  p2 = 1._prec
  m2 = 30._prec
  d = 0.1_prec
  muUV2 = 14.3_prec

  cmp = -0.016287715475817641737585929081643557620468566325668_prec
  call check_real(B0d(p2,m2,d,1),cmp)


  p2 = 3._prec
  m2 = 170._prec**2
  d = 0.1_prec
  muUV2 = 14.3_prec

  cmp = -0.000051899342988800971500060597345977909634992715277674_prec
  call check_real(B0d(p2,m2,d,1),cmp)

  cmp = -6.6113944280872853654286886124420967634228309862390_prec
  call check_real(B0d(p2,m2,d,0),cmp)

  ! check that linear combinations are computed correctly
  cmp = B0d(p2,m2,d,0) + B0d(p2,m2,d,1)- 300*B0d(p2,m2,d,3)
  call check_real(B0d(p2,m2,d,[cone,cone,cnul, -300*cone]),cmp)

  cmp = -2*B0d(p2,m2,d,1)+ 20._prec*B0d(p2,m2,d,3)
  call check_real(B0d(p2,m2,d,[cnul,-2*cone,cnul, 20*cone]),cmp)

  cmp = 0.00001_prec*B0d(p2,m2,d,2)+ 20000._prec*B0d(p2,m2,d,7)
  call check_real(B0d(p2,m2,d,[cnul,cnul,0.00001_prec*cone,cnul,cnul,cnul,cnul,20000._prec*cone]),cmp)


  p2 = 0.00000001_prec
  m2 = 3._prec
  d = 0._prec
  muUV2 = 1._prec

  cmp = -0.098612290334776356210060054823760266375885633268566_prec
  call check_real(B0d(p2,m2,d,0), cmp, 13)

end program b0
