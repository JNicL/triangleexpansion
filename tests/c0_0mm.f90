!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_0mm.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for C0(-p^2,-p^2(1+\delta),0,m^2,m^2) expansions 
program c0_0mm
  use triangle_reduction, only: C0d_0mm
  use triangle_aux, only: prec,dp,qp
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m2
  real(prec) :: p2,z,d,cmp

  p2 = 3._prec
  z = 1._prec
  m2 = z*p2
  d = 5._prec/100._prec

  cmp = -0.22945815872697505987907404198766135928447430837137_prec
  call check_real(C0d_0mm(p2,m2,d),cmp)

  p2 = 3._prec
  m2 = 170._prec**2
  d = 1._prec

  cmp = -0.000034599382481273248983261846232592073833880908125377_prec
  call check_real(C0d_0mm(p2,m2,d),cmp)

  p2 = 1/10000._prec
  m2 = 3._prec
  d = 5._prec/100._prec

  cmp = -0.33332763901861807355417446589185248337566961573715_prec
  call check_real(C0d_0mm(p2,m2,d),cmp)


end program c0_0mm
