!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_m00.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for C0(-p^2,-p^2(1+\delta),m^2,0,0) expansions 
program c0_m00
  use triangle_reduction, only: C0d_m00
  use triangle_aux, only: prec,dp,qp,muIR2
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m2
  real(prec) :: p2,z,d,cmp

  p2 = 3._prec
  z = 1._prec
  m2 = z*p2
  d = 5._prec/100._prec
  muIR2 = 14.3_prec

  cmp = 0.25425946406401280347843794300102532310522813891852_prec
  call check_real(C0d_m00(p2,m2,d),cmp)


  p2 = 3._prec
  m2 = 170._prec**2
  d = 1._prec
  muIR2 = 14.3_prec

  cmp = -0.00022873846233239828490318148149889553165066680172055_prec
  call check_real(C0d_m00(p2,m2,d),cmp)

  !p2 = 1000._prec
  !m2 = 170._prec**2
  !d = 1.5_prec
  !muIR2 = 14.3_prec
  !
  !cmp = -0.00021860066867341408908801269548852939078630099309416_prec
  !call check_real(C0d_m00(p2,m2,d),cmp)

  p2 = 1000._prec
  m2 = 1._prec
  d = 0.1_prec
  muIR2 = 14._prec

  cmp = -0.0041045948548057778867474975551603632462247065647175_prec
  call check_real(C0d_m00(p2,m2,d),cmp)

  p2 = 10000000._prec
  m2 = 1._prec
  d = 0.1_prec
  muIR2 = 14._prec

  cmp = -1.2892313203571424697629993949399781630949945340672_prec*real(1Q-6,kind=prec)
  call check_real(C0d_m00(p2,m2,d),cmp)


  p2 = 0.0001_prec
  m2 = 3._prec
  d = 0._prec/100._prec
  muIR2 = 14.3_prec

  cmp = 0.85383728859826546656624416377057603231175322920711_prec
  call check_real(C0d_m00(p2,m2,d),cmp)


  d = 5._prec/100._prec
  cmp = 0.85383616045507197543382957844980796763514382282109_prec
  call check_real(C0d_m00(p2,m2,d),cmp)

end program c0_m00
