!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                  C_mmm.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! test the tensor reduction formulas against collier/mathematica
program c_mmm
  use triangle_reduction, only: C_mmm_P12,C_mmm_p1,C_mmm_p1p1,C_mmm_p1P12,C_mmm_P12P12,C_mmm_g,C_mmm_p1p1p1, &
                                C_mmm_p1p1P12,C_mmm_p1P12P12, C_mmm_P12P12P12,C_mmm_gp1,C_mmm_gP12
  use triangle_aux, only: prec,muIR2,muUV2
  use test_aux, only: check_real

  complex(prec) :: m2
  real(prec) :: p2,d,cmp

! test 1
  p2 = 1000._prec
  m2 = 3._prec
  d = 1._prec/1000._prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec

! rank 1
  cmp = 0.0048133100302960307699317031599139226638785508030417_prec
  call check_real(C_mmm_p1(p2,m2,d), cmp)

  cmp = -0.0024063368460442033565671049142499866590680565766618_prec
  call check_real(C_mmm_P12(p2,m2,d), cmp)

! rank 2
  cmp = -0.0043308856796719126049486222978476974634928241381239_prec
  call check_real(C_mmm_p1p1(p2,m2,d), cmp)

  cmp = 0.0021651636647951696393415616326143062387447338930949_prec
  call check_real(C_mmm_p1P12(p2,m2,d), cmp)

  cmp = -0.0014433494007833995450359193692353064616715553632806_prec
  call check_real(C_mmm_P12P12(p2,m2,d), cmp)

  cmp = -1.2372494020498625668207256727637441483932523605596_prec
  call check_real(C_mmm_g(p2,m2,d), cmp)

! rank3
  cmp = 0.0040121516178665008486832044011137429403571454964571_prec
  call check_real(C_mmm_p1p1p1(p2,m2,d), cmp)

  cmp = -0.0020058222277616848812212345129772640327256946357485_prec
  call check_real(C_mmm_p1p1P12(p2,m2,d), cmp)

  cmp = 0.0013371303057341010683976870823608057912483157729896_prec
  call check_real(C_mmm_p1P12P12(p2,m2,d), cmp)

  cmp =-0.0010028097028366571789613355577081343970006932816503_prec
  call check_real(C_mmm_P12P12P12(p2,m2,d), cmp)

  cmp = 0.79897938758665449732268877478260905728261067799858_prec
  call check_real(C_mmm_gp1(p2,m2,d), cmp)

  cmp = -0.39950297437882329523171491109949516402520428891605_prec
  call check_real(C_mmm_gP12(p2,m2,d), cmp)


! ! test 2
  p2 = 100000._prec
  m2 = 1._prec
  d = 1._prec/1000._prec
  muUV2 = 2._prec
  muIR2 = 14.3_prec

! rank 1
  cmp = 0.00010508191986397601871849349258207378190860826559751_prec
  call check_real(C_mmm_p1(p2,m2,d), cmp)

  cmp = -0.000052533039965079452329620454216425471172015358324471_prec
  call check_real(C_mmm_P12(p2,m2,d), cmp)

! rank 2
  cmp = -0.00010008556836988448840829168769357384001014481193418_prec
  call check_real(C_mmm_p1p1(p2,m2,d), cmp)

  cmp = 0.000050035280285131353324814313739958106292524373376152_prec
  call check_real(C_mmm_p1P12(p2,m2,d), cmp)

  cmp = -0.000033354352695764455201578366154420378393298386757264_prec
  call check_real(C_mmm_P12P12(p2,m2,d), cmp)

  cmp = -2.2051320647699672064559392440938905793084847578923_prec
  call check_real(C_mmm_g(p2,m2,d), cmp)

! rank3
  cmp = 0.000096754950887157549409595277626348345988724627805452_prec
  call check_real(C_mmm_p1p1p1(p2,m2,d), cmp)

  cmp = -0.000048370248877376778875075982557939532198461251429254_prec
  call check_real(C_mmm_p1p1P12(p2,m2,d), cmp)

  ! ! unsuccessful test
  ! cmp = 0.000032244424183323032796539086048492060501473820572135_prec
  ! call check_real(C_mmm_p1P12P12(p2,m2,muUV2,d), cmp)

  ! ! unsuccessful test
  ! cmp = -0.000024182234492888388058000792117866939581089337006920_prec
  ! call check_real(C_mmm_P12P12P12(p2,m2,muUV2,d), cmp)

  cmp = 1.4423261136050129920191980665986167874108973410434_prec
  call check_real(C_mmm_gp1(p2,m2,d), cmp)

  cmp = -0.72117693437488932481071650957621065011050463441576_prec
  call check_real(C_mmm_gP12(p2,m2,d), cmp)


! ! test 3
  p2 = 10000000._prec
  m2 = 1._prec
  d = 1._prec/1000._prec
  muUV2 = 2._prec
  muIR2 = 14.3_prec

! rank 1
  ! cmp = 1.5111041338979963584089465904106341679708306982547e-6_prec
  ! call check_real(C_mmm_p1(p2,m2,muUV2,d), cmp)

  ! cmp = -7.5543452953316004277420607679469593843301779812866e-7_prec
  ! call check_real(C_mmm_P12(p2,m2,muUV2,d), cmp)

! ! rank 2
  ! cmp = -1.4611292782687177586797163062370216152908947114941e-6_prec
  ! call check_real(C_mmm_p1p1(p2,m2,muUV2,d), cmp)

  ! cmp = 7.3045126419627542736027699112312440429633762894536e-7_prec
  ! call check_real(C_mmm_p1P12(p2,m2,muUV2,d), cmp)

  ! cmp = -4.8692972509534755028220861854900911503841150375892e-7_prec
  ! call check_real(C_mmm_P12P12(p2,m2,muUV2,d), cmp)

  ! cmp = -3.3563629314557600470517728945576073966839209218935_prec
  ! call check_real(C_mmm_g(p2,m2,muUV2,d), cmp)

! ! rank3
  ! cmp = 1.4278127515341869106136947293534210798256635969158e-6_prec
  ! call check_real(C_mmm_p1p1p1(p2,m2,muUV2,d), cmp)

  ! cmp = -7.1379577580717995968436143604936759961091595698037e-7_prec
  ! call check_real(C_mmm_p1p1P12(p2,m2,muUV2,d), cmp)

  ! cmp = 4.7582699097710187553537444624682660338726010386385e-7_prec
  ! call check_real(C_mmm_p1P12P12(p2,m2,muUV2,d), cmp)

  ! cmp = -3.5685365855761035902950772736171678702937657732294e-7_prec
  ! call check_real(C_mmm_P12P12P12(p2,m2,muUV2,d), cmp)

  ! cmp = 2.2097977450517993601632639770082974972463466053110_prec
  ! call check_real(C_mmm_gp1(p2,m2,muUV2,d), cmp)

  ! cmp = -1.1049127544115768045639546919610776843557589783357_prec
  ! call check_real(C_mmm_gP12(p2,m2,muUV2,d), cmp)


! ! test 4
  p2 = 1._prec/10000._prec
  m2 = 170._prec**2
  d = 1._prec/1000._prec
  muUV2 = 2._prec
  muIR2 = 14.3_prec

! ! rank 1
!   cmp = 0.000011534025368866313066931748526288643598006177761284_prec
!   call check_real(C_mmm_p1(p2,m2,muUV2,d), cmp)

!   cmp = -5.7670126844326576569364778805051713865149576138358e-6_prec
!   call check_real(C_mmm_P12(p2,m2,muUV2,d), cmp)

end program c_mmm
