!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                              b0trunc_m0m1.f90                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program b0trunc_m0m1
  use b0_m0m1, only: b0_n_m0m1
  use triangle_aux, only: prec,dp,qp,cnul,cima,muUV2
  use test_aux, only: check_real, check_complex
  implicit none

  complex(prec) :: m02,m12
  complex(prec) :: p2,z1,z2,cmpc
  real(prec) :: cmp

  p2 = 0.2_prec
  z1 = 0.1_prec
  z2 = 0.001_prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 100._prec

  cmp = -0.7547538974104596768893039047030582_prec
  call check_real(B0_n_m0m1(p2,[m02,m12],1),cmp)

  cmp = 0.01528539306900871398437909082209520_prec
  if (prec .eq. qp) then
    call check_real(B0_n_m0m1(p2,[m02,m12],10),cmp, 30)
  else
    call check_real(B0_n_m0m1(p2,[m02,m12],10),cmp, 12)
  end if

  p2 = 4/10._prec
  z1 = 10._prec
  z2 = 1/200._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 100._prec

  cmp = -0.04663920752541411449167454614008825_prec
  call check_real(B0_n_m0m1(p2,[m02,m12],1),cmp)

  cmp = 3.275686248669306573068600748818695_prec*real(1Q-13,kind=prec)
  if (prec .eq. qp) then
    call check_real(B0_n_m0m1(p2,[m02,m12],10),cmp, 19)
  else
    call check_real(B0_n_m0m1(p2,[m02,m12],10),cmp, 1)
  end if

  p2 = 30._prec
  z1 = 1/200._prec
  z2 = 12._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 100._prec

  cmp = -0.03929745763653759763660872431416560_prec
  call check_real(B0_n_m0m1(p2,[m02,m12],1),cmp)

  cmp = 1.651140313427975389566793660583445_prec*real(1Q-11,prec)
  if (prec .eq. qp) then
    call check_real(B0_n_m0m1(p2,[m02,m12],8),cmp,20)
  else
    call check_real(B0_n_m0m1(p2,[m02,m12],8),cmp,5)
  end if

  ! collier checks
  p2 = 1._prec
  z1 = 1._prec
  z2 = 1._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 1._prec

  cmp = -0.15204470482001997_prec
  call check_real(B0_n_m0m1(p2,[m02,m12],0),cmp,14)

  p2 = 10._prec
  z1 = 2._prec-cima/10._prec
  z2 = 3._prec-cima/11._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 134._prec

  ! collier:  1.6203925145557094 + I*0.0363870109547894949
  cmpc = cmplx(1.620392514555709880507500788783452_prec, &
               0.03638701095478950893602672540046478_prec,prec)
  call check_complex(B0_n_m0m1(p2,[m02,m12],0),cmpc)

  end program b0trunc_m0m1
