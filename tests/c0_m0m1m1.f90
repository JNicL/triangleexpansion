!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                c0_m0m1m1.f90                                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: October 12, 2018

! checks for C0(-p^2,-p^2(1+\delta),m0^2,m1^2,m1^2) expansions 
program c0_m0m1m1
  use triangle_reduction, only: C0d_m0m1m1
  use triangle_aux, only: prec,dp,qp
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m02,m12
  real(prec) :: p2,z1,z2,d,cmp

  p2 = 3._prec
  z1 = 1._prec
  z2 = 33._prec
  m02 = z1*p2
  m12 = z2*p2
  d = 5._prec/100._prec

  write(*,*) "z1:", z1
  write(*,*) "z2:", z2

  !cmp = -0.22945815872697505987907404198766135928447430837137_prec
  !call check_real(C0d_m0m1m1(p2,m02,m12,d),cmp)


  p2 = 300._prec
  z1 = 1._prec
  z2 = 0.007_prec
  m02 = z1*p2
  m12 = z2*p2
  d = 1._prec/100._prec

  cmp = -0.008291700349537726259745768189418189_prec
  call check_real(C0d_m0m1m1(p2,m02,m12,d),cmp)

  d = 1._prec/10._prec
  !cmp = -0.008159374727262881218315936277930721_prec
  !cmp = -0.0081593745691470587068349173703063_prec
  cmp = -0.0081593747272628817374710858137850_prec
  cmp = -0.0081593747272628812183159362779307_prec
  cmp = -0.0081593747272628812174351347614928_prec
  if (prec .eq. qp) then
    call check_real(C0d_m0m1m1(p2,m02,m12,d),cmp,21)
  end if

end program c0_m0m1m1
