!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                             c0trunc_m0m1m1.f90                              !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program c0trunc_m0m1m1
  use c0_m0m1m1, only: C0_n_m0m1m1
  use triangle_aux, only: prec,dp,qp,cnul
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m02,m12
  complex(prec) :: p2,z1,z2
  real(prec) :: cmp

  p2 = 3._prec
  z1 = 1._prec
  z2 = 33._prec
  m02 = z1*p2
  m12 = z2*p2

  cmp = -0.009161592326976372937533031993409425_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],0),cmp)

  cmp = 0.00005750187836004546432851289714688108_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],1),cmp)

  if (prec .eq. qp) then
    cmp = -6.170456231051676034481484767329246_prec*real(1Q-7,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],2),cmp,28)

    cmp = 8.192368205033263668425322409627066_prec*real(1Q-9,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],3),cmp,26)

    cmp = -1.219091331828976398787090926310642_prec*real(1Q-10,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],4),cmp,24)

    cmp = -3.400507764382091974821536824458820_prec*real(1Q-21,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],10),cmp,13)
  else
    cmp = -6.170456231051676034481484767329246_prec*real(1Q-7,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],2),cmp,11)

    cmp = 8.192368205033263668425322409627066_prec*real(1Q-9,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],3),cmp,9)

    cmp = -1.219091331828976398787090926310642_prec*real(1Q-10,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],4),cmp,8)
  end if

  p2 = 300._prec
  z1 = 1._prec
  z2 = 0.007_prec
  m02 = z1*p2
  m12 = z2*p2

  cmp = -0.008306792904461819382048761940893761_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],0),cmp)

  cmp = 0.001513273364495645794119818108175126_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],1),cmp)

  cmp = -0.0004030359356941430422471040107627818_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],2),cmp)

  cmp = 0.0001252953343206945969640784014959081_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],3),cmp)

  cmp = -0.00004241383773143743365757826211164175_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],4),cmp)

  cmp = 0.00001515738540478383534515298182105260_prec
  call check_real(C0_n_m0m1m1(p2,[m02,m12],5),cmp)

  if (prec .eq. qp) then
    cmp = -1.332001241405332959691176374637576_prec*real(1Q-7,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],10),cmp,28)
  else
    cmp = -1.332001241405332959691176374637576_prec*real(1Q-7,kind=prec)
    call check_real(C0_n_m0m1m1(p2,[m02,m12],10),cmp,10)
  end if

end program c0trunc_m0m1m1
