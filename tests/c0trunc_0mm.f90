!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                               c0trunc_0mm.f90                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for C0^(n)(-p^2,-p^2,0,m^2,m^2) coefficients
program c0trunc_0mm
  use c0_0mm, only: C0_n_0mm_small_z,C0_n_exp_0mm,C0_n_exp_0mm
  use triangle_aux, only: prec,dp,qp
  use test_aux, only: check_real
  implicit none

  complex(prec) :: z,p2
  real(prec) :: cmp

  p2 = 3.0_prec

  ! transition region z <-> 1/z, loss 4-5 digits
  z = 3._prec
  cmp = 5.1461537100513404806862842514531467069288386098815_prec*real(1Q-17,kind=prec)
  if (prec .eq. dp) then
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,10)
  else
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,29)
  end if

  z = 5._prec
  cmp = 6.2252406191950812958418669375056011565457652115998_prec*real(1Q-21,kind=prec)
  if (prec .eq. dp) then
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,12)
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,12)
  else
    call check_real(C0_n_exp_0mm(z,p2,21),cmp)
  end if

  ! expansion well-behaved for z>=10, full QP 
  z = 10._prec
  cmp = 9.2663832561881552967421752032326381768707185433339_prec*real(1Q-27,kind=prec)
  call check_real(C0_n_exp_0mm(z,p2,21),cmp)

  ! threshold where expansion is more stable
  z = 2._prec
  cmp = 3.2232296742626085937989691919885207659328321970902_prec*real(1Q-14,kind=prec)
  if (prec .eq. dp) then
    call check_real(C0_n_0mm_small_z(z,p2,21),cmp,5)
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,7)
  else
    call check_real(C0_n_0mm_small_z(z,p2,21),cmp,23)
    call check_real(C0_n_exp_0mm(z,p2,21),cmp,25)
  end if

  ! Exact result almost numerically stable, expansion fails
  z = 3._prec/10._prec
  cmp = 8.2181406554886391324485390243582316737415154662990_prec*real(1Q-6,kind=prec)
  if (prec .eq. dp) then
    call check_real(C0_n_0mm_small_z(z,p2,21),cmp,13)
  else
    call check_real(C0_n_0mm_small_z(z,p2,21),cmp,30)
  end if

end program c0trunc_0mm
