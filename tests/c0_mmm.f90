!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_mmm.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for C0(-p^2,-p^2(1+\delta),m^2,m^2,m^2) expansions 
program c0_mmm
  use triangle_reduction, only: C0d_mmm
  use triangle_expansion, only: C0d_mmm_table
  use triangle_aux, only: prec,dp,qp
  use test_aux, only: check_real
  implicit none

  real(prec) :: p2,z,d,cmp
  complex(prec) :: m2

  p2 = 3._prec
  m2 = 170._prec**2

  d = 0._prec
  ! collier: -1.73007387425799570E-005
  cmp = -0.000017300738742579945124059450693342474906763793816028_prec
  call check_real(C0d_mmm(p2,m2,d),cmp)
  call check_real(C0d_mmm_table(p2,m2,d),cmp)

  d = 5._prec/100._prec
  ! collier: -1.73007312597478846E-005
  cmp = -0.000017300731259747881187342874801693746210089437131505_prec
  call check_real(C0d_mmm(p2,m2,d),cmp)
  call check_real(C0d_mmm_table(p2,m2,d),cmp)

  d = 1._prec/2._prec
  ! collier: -1.73006639147252563E-005
  cmp = -0.000017300663914725348798622325179359822575066044828841_prec
  call check_real(C0d_mmm(p2,m2,d),cmp)
  call check_real(C0d_mmm_table(p2,m2,d),cmp)

  p2 = 10000._prec
  m2 = 3._prec

  d = 0._prec
  ! collier: -8.10746479308079547E-004,-6.71507957609130332E-020
  cmp = -0.00081074647930807956500493592591474415535577554189114_prec
  call check_real(C0d_mmm(p2,m2,d),cmp)
  call check_real(C0d_mmm_table(p2,m2,d),cmp)

  d = 1._prec/10._prec
  ! collier: -7.77282134101092443E-004
  cmp = -0.00077728213410110247589421733020308155405818100893598_prec
  !call check_real(C0d_mmm(p2,m2,d),cmp)
  call check_real(C0d_mmm_table(p2,m2,d),cmp)

end program c0_mmm
