!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 b0_m0m1.f90                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: October 22, 2018

program b0_m0m1
  use triangle_reduction, only: B0d_m0m1
  use triangle_aux, only: prec,dp,qp,muUV2
  use test_aux, only: check_real,check_complex
  implicit none

  complex(prec) :: m02,m12,z1,z2,cmpc
  real(prec) :: p2,d,cmp

  p2 = 10._prec
  z1 = 1._prec
  z2 = 1._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 1._prec
  d = 2/100._prec

  ! collier :
  !     -2.4574089123537100
  cmp = -2.457408912353710354972822898244577_prec
  call check_real(B0d_m0m1(p2,m02,m12,d),cmp)

  p2 = 10._prec
  z1 = 1._prec
  z2 = 1._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 1._prec
  d = 2/10._prec

  ! collier :
  !     -2.4820226041881317
  cmp = -2.482022604188130993899005671431180_prec
  call check_real(B0d_m0m1(p2,m02,m12,d),cmp)


  p2 = 11._prec
  z1 = 13._prec
  z2 = 1/7._prec
  m02 = z1*p2
  m12 = z2*p2
  muUV2 = 122._prec
  d = 2/10._prec

  ! collier :
  !     0.74923145234687694
  cmp = 0.7492314523467197611871743491103875_prec
  call check_real(B0d_m0m1(p2,m02,m12,d),cmp)

end program b0_m0m1
