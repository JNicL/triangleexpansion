!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                             auxillaryfuncs.f90                              !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! test auxillary functions
program main
  use triangle_aux, only: choose,i8,prec,qp,HarmNum,Lphi,zlogzf,SvM1, &
                          Sv1,A0mB0,cone
  use test_aux, only: check_integer,check_real
  implicit none

  real(prec) :: cmp,w
    
  ! Binomial coefficient
  call check_integer(int(choose(23,10),kind=i8), 1144066_i8)
  call check_integer(int(choose(4,4),kind=i8), 1_i8)
  call check_integer(int(choose(4,5),kind=i8), 0_i8)
  call check_integer(int(choose(5,-1),kind=i8), 0_i8)
  call check_integer(int(choose(33,21),kind=i8), 354817320_i8)
  if (prec .eq. qp) then
    call check_integer(int(choose(71,21),kind=i8), 547324136192795676_i8)
  end if

  ! Harmonic numbers
  cmp = 4.0887982257395504057901412105564398737696239549527_prec
  call check_real(HarmNum(33), cmp)

  ! LerchPhi(v,1,n+2)
  cmp = 0.44755791892043831340635411753742668744374143348734_prec
  call check_real(Lphi(cone/3._prec,3), cmp)

  ! loss of digits for v~1
  cmp = 0.84988685544468286468314624294916400360692043078838_prec
  if (prec .eq. qp) then
    call check_real(Lphi(cone*10/11._prec,7), cmp, 30)
  else
    call check_real(Lphi(cone*10/11._prec,7), cmp, 14)
  end if

  cmp = -12.512930464953561836756123943421797228660267959028_prec
  call check_real(zlogzf(cone*100000._prec), cmp)

  cmp = -12.512930464953561836756123943421797228660267959028_prec/100000._prec
  call check_real(zlogzf(cone/100000._prec), cmp)

  cmp = -9.0065342158014806942036451104777152137406672157634_prec
  call check_real(zlogzf(cone*3000._prec), cmp)

  cmp = -0.056101536021580677357057340213390633257588435812035_prec
  call check_real(zlogzf(cone/100._prec), cmp)

  cmp = -0.74978019282507780038404042029927847688717646713793_prec
  call check_real(zlogzf(cone/3._prec), cmp)

  cmp = -0.74978019282507780038404042029927847688717646713793_prec*3
  call check_real(zlogzf(cone*3._prec), cmp)

  cmp = -0.95477125244221922767563573392561198889573570237387_prec
  call check_real(zlogzf(cone/2._prec), cmp)

  cmp = -0.95477125244221922767563573392561198889573570237387_prec*2
  call check_real(zlogzf(cone*2._prec), cmp)

  cmp = -1.9054794972483211814099941372056715755449658106151_prec
  call check_real(zlogzf(cone*1.99_prec), cmp)

  cmp = -0.55067101790962068262838691305128527271250051842911_prec
  call check_real(SvM1(cone*0.0000001_prec,8000000),cmp)

  cmp = -0.99966453723791241910883304961980013495775159749996_prec
  if (prec .eq. qp) then
    call check_real(SvM1(cone*0.0000001_prec,80000000),cmp,30)
  else
    call check_real(SvM1(cone*0.0000001_prec,80000000),cmp, 12)
  end if

  if (prec .eq. qp) then
    cmp = 2979.9460632415769234700531356161509119295055320214_prec
    call check_real(Sv1(cone*0.000001_prec,8000000),cmp,27)
  end if

  cmp = 0.00080031968503054165683975111124928422294837451147219_prec
  call check_real(Sv1(cone*0.000001_prec,800),cmp)

  cmp = -1.1110925929629547327062463258177799068526157636773_prec*real(1Q-9,prec)
  call check_real(A0mB0(0.0001_prec,3._prec*cone),cmp)

  cmp = -0.0016262097677117425085500452541686769302121890910776_prec
  call check_real(A0mB0(0.1_prec,2._prec*cone),cmp)

end program main
