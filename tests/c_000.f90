!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                  c_000.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! test the tensor reduction formulas against collier/mathematica
program c_000
  use triangle_reduction, only: C_000_P12,C_000_p1, B0d_0, C0d_000, C_000_p1p1, C_000_P12P12,C_000_p1P12, C_000_g, &
                                C_000_p1p1p1, C_000_p1p1P12, C_000_p1P12P12, C_000_P12P12P12, C_000_gp1, C_000_gP12
  ! use triangle_expansion, only: 
  use triangle_aux, only: prec,muIR2,muUV2
  use test_aux, only: check_real

  complex(prec) :: m2, C0(0:1)
  real(prec) :: p2,d,cmp

! ----------------------- test1 --------------
  p2 = 1._prec
  ! m2 = 3._prec
  d = 1._prec/100._prec
  muUV2 = 1._prec
  muIR2 = 1._prec
  ! print *, 'here'
  ! write(*,*) "B0d(p2,muUV2,d,0):", B0d_0(p2,muUV2,d,0)
  ! write(*,*) "B0d(p2,muUV2,d,1):", B0d_0(p2,muUV2,d,1)
  ! write(*,*) "B0d_0(p2, muUV2, d,1)/p2",B0d_0(p2, muUV2, d,1)/p2 
  ! write(*,*) "C_000_p1(p2,d)", C_000_p1(p2,d)
  ! write(*,*) "C_000_P12(p2,d)", C_000_P12(p2,d)
  ! write(*,*) "B0d_0(p2, muUV2, d,1)/p2 - C0d_000(p2,muIR2,d)", B0d_0(p2, muUV2, d,1)/p2 - C0d_000(p2,muIR2,d)

  ! C010 converted to the one with p1,P12 parametrization
  C0 = C_000_p1(p2,d)
  cmp = -0.99008263111243285144603860301777256241166770754867_prec
  call check_real(C0(0), cmp)
  ! C001 converted to the one with p1,P12 parametrization
  C0 = C_000_P12(p2,d)
  cmp = 0.49339527175907808842102850118925270316916885053949_prec
  call check_real(C0(0), cmp)

  ! C020 converted to the one with p1,P12 parametrization
  C0 = C_000_p1p1(p2,d)
  cmp = 1.4875991737708369938568064802308096468456482045516_prec
  call check_real(C0(0), cmp)
  ! C002 converted to the one with p1,P12 parametrization
  C0 = C_000_P12P12(p2,d)
  cmp = 0.49380944951663523084392626460338127825907450784549_prec
  call check_real(C0(0), cmp)
  ! C011 converted to the one with p1,P12 parametrization
  cmp = -0.74174100591866384734424077988554425977111915024551_prec
  C0 = C_000_p1P12(p2,d)
  call check_real(C0(0), cmp)
  ! C200 converted to the one with p1,P12 parametrization
  cmp = 0.49875414595750590808256222200741627236083984901352_prec
  C0 = C_000_g(p2,d)
  call check_real(C0(0), cmp)

  ! C030 converted to the one with p1,P12 parametrization
  ! write (*,*) "C_000_p1p1p1(p2,d)",C_000_p1p1p1(p2,d)
  C0 = C_000_p1p1p1(p2,d)
  cmp = -1.8192768688764397554639850650395010364683018692202_prec
  call check_real(C0(0), cmp)
  ! C021 converted to the one with p1,P12 parametrization
  C0 = C_000_p1p1P12(p2,d)
  cmp = 0.90730482869172101995971563234973863083908601671618_prec
  call check_real(C0(0), cmp)
  ! C012 converted to the one with p1,P12 parametrization
  C0 = C_000_p1P12P12(p2,d)
  cmp = -0.60409383887758463596310768485061083812905452744478_prec
  call check_real(C0(0), cmp)
  ! C003 converted to the one with p1,P12 parametrization
  C0 = C_000_P12P12P12(p2,d)
  cmp = 0.45272170326321535238613312762949103269611385314709_prec
  call check_real(C0(0), cmp)
  ! C110 converted to the one with p1,P12 parametrization
  C0 = C_000_gp1(p2,d)
  cmp = -0.36028054174944838316615259244938862601833767712012_prec
  call check_real(C0(0), cmp)
  ! C110 converted to the one with p1,P12 parametrization
  C0 = C_000_gP12(p2,d)
  cmp = 0.18000207229119392216433556264415190098117387144288_prec
  call check_real(C0(0), cmp)


! ----------------------- test2 --------------
  p2 = 3._prec
  d = 1._prec/10000._prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec
  ! print *, 'here'
  ! write(*,*) "B0d(p2,muUV2,d,0):", B0d_0(p2,muUV2,d,0)
  ! write(*,*) "B0d(p2,muUV2,d,1):", B0d_0(p2,muUV2,d,1)
  ! write(*,*) "B0d_0(p2, muUV2, d,1)/p2",B0d_0(p2, muUV2, d,1)/p2 
  ! write(*,*) "C_000_p1(p2,d)", C_000_p1(p2,d)
  ! write(*,*) "C_000_P12(p2,d)", C_000_P12(p2,d)
  ! write(*,*) "B0d_0(p2, muUV2, d,1)/p2 - C0d_000(p2,muIR2,d)", B0d_0(p2, muUV2, d,1)/p2 - C0d_000(p2,muIR2,d)

  ! C010 converted to the one with p1,P12 parametrization
  C0 = C_000_p1(p2,d)
  cmp = -0.85382305992434917561517339425843393805852755465434_prec
  call check_real(C0(0), cmp)
  ! C001 converted to the one with p1,P12 parametrization
  C0 = C_000_P12(p2,d)
  cmp = 0.42690163762572063224722985331470772891337723755323_prec
  call check_real(C0(0), cmp)

  ! C020 converted to the one with p1,P12 parametrization
  C0 = C_000_p1p1(p2,d)
  cmp = 1.0204813938131964011704511958436523771953015564395_prec
  call check_real(C0(0), cmp)
  ! C002 converted to the one with p1,P12 parametrization
  C0 = C_000_P12P12(p2,d)
  cmp = 0.34014918360239369945377927842816785808579604696549_prec
  call check_real(C0(0), cmp)
  ! C011 converted to the one with p1,P12 parametrization
  C0 = C_000_p1P12(p2,d)
  cmp = -0.51022941582013174613588066779698302784002605296135_prec
  call check_real(C0(0), cmp)
  ! C200 converted to the one with p1,P12 parametrization
  C0 = C_000_g(p2,d)
  cmp = 0.22533442824961841173443869672130313236709584176636_prec
  call check_real(C0(0), cmp)

  ! C030 converted to the one with p1,P12 parametrization
  ! write (*,*) "C_000_p1p1p1(p2,d)",C_000_p1p1p1(p2,d)
  C0 = C_000_p1p1p1(p2,d)
  cmp = -1.1315869497390945515406363969004646699531508909629_prec
  call check_real(C0(0), cmp)
  ! C021 converted to the one with p1,P12 parametrization
  C0 = C_000_p1p1P12(p2,d)
  cmp = 0.56578126794973915539498121078516656045779192990010_prec
  call check_real(C0(0), cmp)
  ! C012 converted to the one with p1,P12 parametrization
  C0 = C_000_p1P12P12(p2,d)
  cmp = -0.37718344308385666400390495214839723598258221503897_prec
  call check_real(C0(0), cmp)
  ! C003 converted to the one with p1,P12 parametrization
  C0 = C_000_P12P12P12(p2,d)
  cmp = 0.28288575134289560805799454626000340805767388852046_prec
  call check_real(C0(0), cmp)
  ! C110 converted to the one with p1,P12 parametrization
  C0 = C_000_gp1(p2,d)
  cmp = -0.17800072994419005226740357559197986602250833895535_prec
  call check_real(C0(0), cmp)
  ! C110 converted to the one with p1,P12 parametrization
  C0 = C_000_gP12(p2,d)
  cmp = 0.088998976152646415300348614680794836449650095999509_prec
  call check_real(C0(0), cmp)

 
end program c_000
