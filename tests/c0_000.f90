!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_000.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for C0(-p^2,-p^2(1+\delta),m^2,0,0) expansions 
program c0_000
  use triangle_reduction, only: C0d_000, B0d_0
  use triangle_aux, only: prec,dp,qp,muIR2
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m2
  real(prec) :: p2,z,d,cmp, muUV2

! ---------- test1 ---------------
  p2 = 1._prec
  d = 1._prec/100._prec
  muIR2 = 1._prec
  muUV2 = 1._prec

  ! check massless B0d_0 function
  cmp = 1.9900496691468319171517846424557392583113203900599_prec
  call check_real(B0d_0(p2,d,0),cmp)
  ! check C0_000 function
  cmp = -0.0049504542043754333754971514083016064562932864572133_prec
  call check_real(C0d_000(p2,d),cmp)
  

! ---------- test1 ---------------
  p2 = 3._prec
  d = 1._prec/10000._prec
  muIR2 = 14.3_prec
  muUV2 = 1._prec
  ! write(*,*) "here ............."
  ! write(*,*) "C0d_000(p2,d):", C0d_000(p2,d)
  ! write(*,*) "here ............."
  ! write(*,*) "C0d_000(p2,d):", C0d_000(p2,d,1)

  ! check massless B0d_0 function
  cmp = 0.90128771633155700026942159639652316428902737777618_prec
  call check_real(B0d_0(p2,d,0),cmp)
  ! check C0_000 function
  cmp = 0.52050639214665472450461779108799705978497955108408_prec
  call check_real(C0d_000(p2,d),cmp)
  

end program c0_000
