!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                   b0.f90                                    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

program b0_mm
  use triangle_expansion, only: B0d_mm_opt, B0d_mm_opt_list
  use triangle_aux, only: prec,dp,qp,cone,muUV2
  use test_aux, only: check_real
  implicit none

  complex(prec) :: m2
  real(prec) :: p2,z,d,cmp
  real :: t1, t2

  p2 = 100._prec
  m2 = 1._prec
  muUV2 = 14.3_prec
  d = 0._prec/1000._prec
  cmp = -0.056207753186097297893324652698205027115397928346724_prec
  call check_real(B0d_mm_opt(p2,m2,d,0),cmp)

  muUV2 = 14._prec
  d = 1._prec/1000._prec
  ! p2 = 1000._prec
  cmp = -0.078318840016452895003109977699730581591641721209997_prec
  !call check_real(B0d_mm_coeff(p2,m2,muUV2,d,0),cmp)
  ! call check_real(B0d_mm_explicit(p2,m2,muUV2,d,0),cmp)
  call check_real(B0d_mm_opt(p2,m2,d,0),cmp)
  call check_real(B0d_mm_opt_list(p2,m2,d,[cone]),cmp)

  muUV2 = 14._prec
  d = 1._prec/1000._prec
  p2 = 1._prec/1000._prec
  m2 = 170._prec**2
  cmp = -7.6325395502580445941328163532610822964638776221832_prec
  ! call check_real(B0d_mm_explicit(p2,m2,muUV2,d,0),cmp)
  !call check_real(B0d_mm_coeff(p2,m2,muUV2,d,0),cmp)
  call check_real(B0d_mm_opt(p2,m2,d,0),cmp)

   !muUV2 = 14._prec
   !d = 1._prec/100000._prec
   !p2 = 1._prec/1000._prec
   !m2 = 170._prec**2
   !cmp = -7.6325395445429927110116211836232458828233297950802_prec
   !call check_real(B0d_mm_coeff(p2,m2,muUV2,d,0),cmp)
   !call check_real(B0d_mm_opt(p2,m2,muUV2,d,0),cmp)

  ! d = 0.7_prec
  ! call CPU_TIME( t1 )
  ! call check_real(B0d_mm_coeff(p2,m2,muUV2,d,0),cmp)
  ! call CPU_TIME( t2 )
  ! print *, "time for recursive:", t2-t1

  ! call CPU_TIME( t1 )
  ! call check_real(B0d_mm_opt(p2,m2,muUV2,d,0),cmp)
  ! ! call check_real(B0d_mm_list(p2,m2,muUV2,d,[1._prec]),cmp)
  ! call CPU_TIME( t2 )
  ! print *, "time for opt:", t2-t1

end program b0_mm
