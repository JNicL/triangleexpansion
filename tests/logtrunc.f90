!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                logtrunc.f90                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! checks for log(1+x) expansions
program logtrunc
  use triangle_aux, only: prec,log1pX,cone
  use test_aux, only: check_real
  implicit none

  real(prec) :: cmp
    
  cmp = log(1.4_prec)
  call check_real(log1pX(0.4_prec*cone,0),cmp)

  cmp = -0.063527763378787069495406589783007909888516624686657_prec
  call check_real(log1pX(0.4_prec*cone,1),cmp)

  cmp = 9.0825834427472531631362099031908265404322738084634_prec*real(1Q-35,kind=prec)
  call check_real(log1pX(0.001_prec*cone,10),cmp)

end program logtrunc
