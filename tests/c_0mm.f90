!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                  c_0mm.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! test the tensor reduction formulas against collier/mathematica
program c_0mm
  use triangle_reduction, only: C_0mm_P12,C_0mm_p1,C_0mm_p1p1,C_0mm_p1P12,C_0mm_P12P12, C_0mm_g, C_0mm_p1p1p1, C_0mm_p1p1P12,&
                                 C_0mm_p1P12P12, C_0mm_P12P12P12, C_0mm_gp1, C_0mm_gP12
  use triangle_aux, only: prec,muUV2
  use test_aux, only: check_real

  complex(prec) :: m2
  real(prec) :: p2,d,muIR2,cmp

! test 1
  p2 = 3._prec
  m2 = 3._prec
  d = 5._prec/100._prec
  !muUV2 = 94.7777_prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec

! rank 1
  cmp = 0.12810897280783657347609544787778089532093929444820_prec
  call check_real(C_0mm_p1(p2,m2,d), cmp)

  cmp = -0.063945696817282068085217311280786597324729931059520_prec
  call check_real(C_0mm_P12(p2,m2,d), cmp)

! rank 2
  cmp = -0.21921449500943036061615013022033226652539556276061_prec
  call check_real(C_0mm_g(p2,m2,d), cmp)

! rank3
  cmp = 0.070369673621802828107579827646888893366694850006655_prec
  call check_real(C_0mm_p1p1p1(p2,m2,d), cmp)

  cmp = -0.035144141159956192137906009233952234914986500598734_prec
  call check_real(C_0mm_p1p1P12(p2,m2,d), cmp)

  cmp = 0.023415890653780636776183744861346723718156781344699_prec
  call check_real(C_0mm_p1P12P12(p2,m2,d), cmp)

  cmp = -0.017555834942566783580382179930883396257363741849703_prec
  call check_real(C_0mm_P12P12P12(p2,m2,d), cmp)

  cmp = 0.16360993289569637853422288131915864777524829159987_prec
  call check_real(C_0mm_gp1(p2,m2,d), cmp)

  cmp = -0.081930714124220641826850256870942483159960271873769_prec
  call check_real(C_0mm_gP12(p2,m2,d), cmp)

! ------ modified muUV2 tests --------
! rank 2
  muUV2 = 12._prec/33._prec
  cmp = -0.090489428001041165135801144566763162405524721741136_prec
  call check_real(C_0mm_p1p1(p2,m2,d), cmp)

  cmp = 0.045182715776588371079679001723425828250116853308622_prec
  call check_real(C_0mm_p1P12(p2,m2,d), cmp)

  cmp = -0.030101193900480747458254971714469403124323772085614_prec
  call check_real(C_0mm_P12P12(p2,m2,d), cmp)


! test 2
  p2 = 100._prec
  m2 = 1._prec
  d = 1._prec/1000._prec
  muUV2 = 14.3_prec
  muIR2 = 1._prec

! rank 2
  cmp = -0.031967494955024179211413676495149540331976448391792_prec
  call check_real(C_0mm_p1p1(p2,m2,d), cmp)

  cmp = -0.010653940689745784711200807940913211818108908474001_prec
  call check_real(C_0mm_P12P12(p2,m2,d), cmp)

  cmp = 0.015981856413779771021864612658255403527540993850724_prec
  call check_real(C_0mm_p1P12(p2,m2,d), cmp)

  cmp = -0.0095220359694835755365286364836739691311314071238569_prec
  call check_real(C_0mm_g(p2,m2,d), cmp)

! rank3
  cmp = 0.028955342415524741694546255918420399967165882886270_prec
  call check_real(C_0mm_p1p1p1(p2,m2,d), cmp)

  cmp = -0.014476012130125746670088621005557838324537649957841_prec
  call check_real(C_0mm_p1p1P12(p2,m2,d), cmp)

  cmp = 0.0096501218149011800391719205543657834301960731979710_prec
  call check_real(C_0mm_p1P12P12(p2,m2,d), cmp)

  cmp = -0.0072373425650478736150879719410161318198835743237363_prec
  call check_real(C_0mm_P12P12P12(p2,m2,d), cmp)

  cmp = -0.016101837972284697551523074039470208301694098296574_prec
  call check_real(C_0mm_gp1(p2,m2,d), cmp)

  cmp = 0.0080383683509221132837678594130813507304697864328659_prec
  call check_real(C_0mm_gP12(p2,m2,d), cmp)





  ! JNL checks
  p2 = 1/10000._prec
  m2 = 3._prec
  d = 0._prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec

  cmp = 0.16666481484567839507544548958945697036682191230706_prec
  call check_real(C_0mm_p1(p2,m2,d), cmp)
  cmp = -0.083332407422839197537722744794728485183410956153530_prec
  call check_real(C_0mm_P12(p2,m2,d), cmp)

  cmp = -0.11111018519753065844013317329856797502419112412319_prec
  call check_real(C_0mm_p1p1(p2,m2,d), cmp)
  cmp = 0.055555092598765329220066586649283987512095562061597_prec
  call check_real(C_0mm_p1P12(p2,m2,d), cmp)
  cmp = -0.037036728399176886146711057766189325008063708041065_prec
  call check_real(C_0mm_P12P12(p2,m2,d), cmp)
  cmp = -0.14965584992165736111527231255996389036124824423379_prec
  call check_real(C_0mm_g(p2,m2,d), cmp)
  cmp = 0.083332777783950529101998797004485367424581486637717_prec
  call check_real(C_0mm_p1p1p1(p2,m2,d), cmp)
  cmp = -0.041666388891975264550999398502242683712290743318858_prec
  call check_real(C_0mm_p1p1P12(p2,m2,d), cmp)
  cmp = 0.027777592594650176367332932334828455808193828879239_prec
  call check_real(C_0mm_p1P12P12(p2,m2,d), cmp)
  cmp = -0.020833194445987632275499699251121341856145371659429_prec
  call check_real(C_0mm_P12P12P12(p2,m2,d), cmp)
  cmp = 0.12754788143542579218580368391148213664181661377301_prec
  call check_real(C_0mm_gp1(p2,m2,d), cmp)
  cmp = -0.063773940717712896092901841955741068320908306886507_prec
  call check_real(C_0mm_gP12(p2,m2,d), cmp)

  p2 = 1/10000._prec
  m2 = 3._prec
  d = 5._prec/100._prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec

  cmp = -0.11111016205001006635592583826292635135442767565969_prec
  call check_real(C_0mm_p1p1(p2,m2,d), cmp)
  cmp = 0.055555077167085791792359417677098969107096345771467_prec
  call check_real(C_0mm_p1P12(p2,m2,d), cmp)
  cmp = -0.037036716825417618858681791905106711826740931703576_prec
  call check_real(C_0mm_P12P12(p2,m2,d), cmp)
  cmp = -0.14965591936492513195770994782586009569436788401801_prec
  call check_real(C_0mm_g(p2,m2,d), cmp)
  cmp = 0.083332763895375419384523964222329102658767963969848_prec
  call check_real(C_0mm_p1p1p1(p2,m2,d), cmp)
  cmp = -0.041666379632925620056687987633189932959019724443762_prec
  call check_real(C_0mm_p1p1P12(p2,m2,d), cmp)
  cmp = 0.027777585650363135889383318831418359262402905037369_prec
  call check_real(C_0mm_p1P12P12(p2,m2,d), cmp)
  cmp = -0.020833188890558102769284447387440053235038645150535_prec
  call check_real(C_0mm_P12P12P12(p2,m2,d), cmp)
  cmp = 0.12754791615717734337199177323759212836234798273074_prec
  call check_real(C_0mm_gp1(p2,m2,d), cmp)
	cmp = -0.063773963865546620568106844584992276957028471038613_prec
  call check_real(C_0mm_gP12(p2,m2,d), cmp)

  p2 = 10000._prec
  m2 = 3._prec
  d = 1._prec/10._prec
  muUV2 = 1._prec
  muIR2 = 14.3_prec

  cmp = -0.00063515209903167243859247505207799988507902269014891_prec
  call check_real(C_0mm_p1p1(p2,m2,d), cmp)
  cmp = 0.00031328589476195930044013034035843809903028771435376_prec
  call check_real(C_0mm_p1P12(p2,m2,d), cmp)
  cmp = -0.00020745208067666067563513743435325496490645848181821_prec
  call check_real(C_0mm_P12P12(p2,m2,d), cmp)
  cmp = -1.8158547934366983444400315245152385451582204411274_prec
  call check_real(C_0mm_g(p2,m2,d), cmp)
  cmp = 0.00060356376544178328207705553095289446567539382995492_prec
  call check_real(C_0mm_p1p1p1(p2,m2,d), cmp)
  cmp = -0.00029774135362461254957676964343239202344858613569941_prec
  call check_real(C_0mm_p1p1P12(p2,m2,d), cmp)
  cmp = 0.00019717068306774739550339812796812346077581951663806_prec
  call check_real(C_0mm_p1P12P12(p2,m2,d), cmp)
  cmp = -0.00014728926769407292484500843327519103556566981911330_prec
  call check_real(C_0mm_P12P12P12(p2,m2,d), cmp)
  cmp = 1.1831096605628702880682061427584202522702420276522_prec
  call check_real(C_0mm_gp1(p2,m2,d), cmp)
	cmp = -0.59287061616354351171700841901443876190743292541186_prec
  call check_real(C_0mm_gP12(p2,m2,d), cmp)

end program c_0mm
