program irpoles
  use triangle_expansion, only: C0d_000_EP1_coeff,C0d_000_coeff,C0d_m00_EP1_coeff,C0d_m00_coeff
  use triangle_aux, only: prec,dp,qp,set_dir,muIR2,muUV2

  use COLLIER

  real(prec) :: p2,d
  complex(prec) :: z,m0,m1

  integer, parameter :: rmax = 3
  double complex :: C(0:rmax/2,0:rmax,0:rmax), C0c
  double complex :: Cuv(0:rmax/2,0:rmax,0:rmax)
  double complex :: p10,p21,p20,m02,m12,m22

  call Init_cll(4,noreset=.true.)
  call SetMode_cll(1)

  muUV2 = 1._prec
  muIR2 = 1._prec
  call SetMuUV2_cll(real(muUV2,8))
  call SetMuIR2_cll(real(muUV2,8))

  d = 0.4_prec
  p2 = 99._prec
  m0 = 0._prec
  m1 = 0._prec

  p10 = complex(-p2,0d0)
  p21 = complex(0d0,0d0)
  p20 = complex(-p2*(1d0+d),0d0)
  m02 = m0
  m12 = m1
  m22 = m1

  call SetDeltaIR_cll(0d0,0d0)
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m12,rmax)
  C0c = C(0,0,0)
  call SetDeltaIR_cll(1d0,0d0)
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m12,rmax)
  C0c = C(0,0,0) - C0c

  call set_dir(1._prec)
  write(*,*) "diff:", diff(C(0,0,0),C0d_000_coeff(p2,d))
  write(*,*) "diff:", diff(C0c,C0d_000_EP1_coeff(p2,d))

  d = 0.3_prec
  p2 = 99._prec
  m0 = 10._prec
  m1 = 0._prec

  p10 = complex(-p2,0d0)
  p21 = complex(0d0,0d0)
  p20 = complex(-p2*(1d0+d),0d0)
  m02 = m0
  m12 = m1
  m22 = m1

  call SetDeltaIR_cll(0d0,0d0)
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m12,rmax)
  C0c = C(0,0,0)
  call SetDeltaIR_cll(1d0,0d0)
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m12,rmax)
  C0c = C(0,0,0) - C0c

  call set_dir(1._prec)
  write(*,*) "diff:", diff(C(0,0,0),C0d_m00_coeff(p2,m0,d))
  write(*,*) "diff:", diff(C0c,C0d_m00_EP1_coeff(p2,m0,d))

  contains

  function diff(c0,c1)
    complex(prec),intent(in) :: c0
    double complex,intent(in) :: c1
    real(prec) :: diff

    diff = abs((c0-c1)/(c0+c1))
    
  end function diff
  
end program irpoles
