program c0trunc_0mm
  use triangle_reduction
  use triangle_aux, only: prec,dp,qp,muIR2,muUV2

  use COLLIER

  real(prec) :: p2,d
  complex(prec) :: z,m0,m1,cmp,loss,B01,B02,B03,B04,B05
  complex(prec) :: C0,C_p1,C_P12
  complex(prec) :: C_p1p1,C_p1P12,C_P12P12, C_g
  complex(prec) :: C_p1p1p1,C_p1P12P12,C_p1p1P12,C_P12P12P12,C_gp1,C_gP12

  double complex :: A01,A02,B0,B0dd
  integer, parameter :: rmax = 3
  double complex :: C(0:rmax/2,0:rmax,0:rmax)
  double complex :: Cuv(0:rmax/2,0:rmax,0:rmax)
  double complex :: p10,p21,p20,m02,m12,m22

  call Init_cll(4,noreset=.true.)
  call SetMode_cll(1)

  muUV2 = 1._prec
  muIR2 = 1._prec
  call SetMuUV2_cll(real(muUV2,8))
  call SetMuIR2_cll(real(muUV2,8))

  d = 0.416_prec
  p2 = 3._prec
  m0 = 10._prec
  m1 = 170._prec

  p10 = complex(-p2,0d0)
  p21 = complex(0d0,0d0)
  p20 = complex(-p2*(1d0+d),0d0)
  m02 = m0
  m12 = m1
  m22 = m1

  call A0_cll(A01,m02)
  call A0_cll(A02,m12)
  call B0_cll(B0dd,p20,m02,m12)
  call B0_cll(B0,p10,m02,m12)
  call C_cll(C,Cuv,p10,p21,p10,m02,m12,m12,0)
  write(*,*) "A01:", A01
  write(*,*) "A02:", A02
  write(*,*) "B0:", B0
  write(*,*) "B0d:", B0dd
  write(*,*) "C0:", C(0,0,0)
  ! C tensor
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m12,rmax)

  write(*,*) "C0d:", C(0,0,0)
  write(*,*)

  C0 = C0d_m0m1m1(p2,m0,m1,d)
  C_p1 = C_m0m1m1_p1(p2,m0,m1,d)
  C_P12 = C_m0m1m1_P12(p2,m0,m1,d)

  C_p1p1 = C_m0m1m1_p1p1(p2,m0,m1,d)
  C_p1P12 = C_m0m1m1_p1P12(p2,m0,m1,d)
  C_P12P12 = C_m0m1m1_P12P12(p2,m0,m1,d)
  C_g = C_m0m1m1_g(p2,m0,m1,d)

  C_p1p1p1 = C_m0m1m1_p1p1p1(p2,m0,m1,d)
  C_p1p1P12 = C_m0m1m1_p1p1P12(p2,m0,m1,d)
  C_p1P12P12 = C_m0m1m1_p1P12P12(p2,m0,m1,d)
  C_P12P12P12 = C_m0m1m1_P12P12P12(p2,m0,m1,d)
  C_gp1 = C_m0m1m1_gp1(p2,m0,m1,d)
  C_gP12 = C_m0m1m1_gP12(p2,m0,m1,d)

  write(*,*) "C0:", diff(C0,C(0,0,0))
  write(*,*) "C1:", diff(C_p1 + C_P12,C(0,1,0))
  write(*,*) "C2:", diff(-C_P12,C(0,0,1))

  write(*,*)
  write(*,*) "C11:", diff(C_p1p1 + C_P12P12 + C_p1P12*2,C(0,2,0))
  write(*,*) "C12:", diff(- C_P12P12 - C_p1P12,C(0,1,1))
  write(*,*) "C22:", diff(C_P12P12,C(0,0,2))
  write(*,*) "Cg :", diff(C_g,C(1,0,0))

  write(*,*)
  write(*,*) "C003 :", diff(-C_P12P12P12,C(0,0,3))
  write(*,*) "C012 :", diff(C_p1P12P12 + C_P12P12P12,C(0,1,2))
  write(*,*) "C021 :", diff(-C_p1p1P12 - 2*C_p1P12P12 - C_P12P12P12,C(0,2,1))
  write(*,*) "C030 :", diff(C_p1p1p1 + 3*C_p1p1P12 + 3*C_p1P12P12 + C_P12P12P12,C(0,3,0))
  write(*,*) "C101 :", diff(-C_gP12,C(1,0,1))
  write(*,*) "C110 :", diff(C_gp1 + C_gP12,C(1,1,0))

  contains

  function diff(c0,c1)
    complex(prec),intent(in) :: c0
    double complex,intent(in) :: c1
    real(prec) :: diff

    diff = abs((c0-c1)/(c0+c1))
    
  end function diff
  
end program c0trunc_0mm
