# File: CMakeLists.txt
# Author: Jean-Nicolas Lang
# Description: CMake configure file for TR
# Last Modified: December 26, 2017

option (static "shared_libraries" OFF)
# FFLAGS depend on the compiler
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)

set (unoptimized "-O0")
if (Fortran_COMPILER_NAME MATCHES "gfortran.*" OR Fortran_COMPILER_NAME MATCHES "f95")
  # gfortran
  set (debug " -g -ffixed-line-length-132")
  set (checks " -fcheck=all -ffpe-trap=invalid,zero,overflow")
  set (inits " -finit-integer=-1000000 -finit-logical=true -finit-real=nan")
  set (warnings " -Wall ")
  set (maths " -fno-fast-math -fno-omit-frame-pointer -ftrapv")
  set (debug "${debug} ${checks} ${inits} ${maths} ${warnings}")
  set (CMAKE_Fortran_FLAGS_DEBUG "${debug}")
  set (CMAKE_Fortran_FLAGS_RELEASE "-ffixed-line-length-132")
  set (optimized "-Ofast")
elseif (Fortran_COMPILER_NAME MATCHES "ifort.*")
  # ifort (untested)
  set (basic " -132")
  set (opt_DEBUG " -O0 -check -check noarg_temp_created -g")
  set (opt_DEBUG "${opt_DEBUG} -warn all -warn nodeclarations -warn nounused")
  set (CMAKE_Fortran_FLAGS_RELEASE "${basic} ${opt_RELEASE}")
  set (CMAKE_Fortran_FLAGS_DEBUG "${basic} ${opt_DEBUG}")
  set (optimized "-fast -ipo")
elseif (Fortran_COMPILER_NAME MATCHES "pgf77")
  # g77 (untested)
  set (basic "${basic} -ffixed-line-length-132  -g77libs -Msecond_underscore")
  set (opt_DEBUG "-O0 -Mbounds -frange-check -g -fcheck=all")
  set (opt_DEBUG "${opt_DEBUG} -Wall -Wtabs -Wextra -Wno-unused")
  set (opt_DEBUG "${opt_DEBUG} -Wno-unused-dummy-argument")
  set (opt_DEBUG "${opt_DEBUG} -Wno-unused-parameter")
  set (CMAKE_Fortran_FLAGS_RELEASE "${basic} ${opt_RELEASE}")
  set (CMAKE_Fortran_FLAGS_DEBUG "${basic} ${opt_DEBUG}")
  set (optimized "-O2 -fast")
else (Fortran_COMPILER_NAME MATCHES "gfortran.*")
  message ("CMAKE_Fortran_COMPILER full path: " ${CMAKE_Fortran_COMPILER})
  message ("Fortran compiler: " ${Fortran_COMPILER_NAME})
  set (CMAKE_Fortran_FLAGS_DEBUG   "-g")
  set (optimized "-O2")
endif ()

# need preprocessor flag support
set (CMAKE_Fortran_FLAGS_DEBUG   "${CMAKE_Fortran_FLAGS_DEBUG} -cpp")
set (CMAKE_Fortran_FLAGS_RELEASE "${CMAKE_Fortran_FLAGS_RELEASE} -cpp")

if(quad_precision)
  remove_definitions(-DPRECISION_dp)  
else()
  add_definitions(-DPRECISION_dp)
endif()

set(SOURCE_FILES "")

SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/triangle_reduction.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/triangle_expansion.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/b0.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/c0_0mm.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/c0_m00.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/c0_000.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/c0_mmm.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/c0_m0m1m1.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/b0_mm.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/b0_m0m1.f90)
SET(SOURCE_FILES ${SOURCE_FILES} ${TR_SOURCE_PATH}/triangle_aux.f90)


string(TOLOWER "${CMAKE_BUILD_TYPE}" CBT)
if (CBT MATCHES debug)
  set(OFLAG ${unoptimized})
else()
  set(OFLAG ${optimized})
endif ()
set_source_files_properties(${SOURCE_FILES} PROPERTIES COMPILE_FLAGS ${OFLAG})

# gather all source files
set(SOURCE ${SOURCE_FILES})
add_library (tr SHARED ${SOURCE_FILES})

message("")
if (CBT MATCHES debug)
  message("Build type: Debug")
else()
  message("Build type: Release")
endif ()


message ("TR:      Fortran compiler       = ${Fortran_COMPILER_NAME}")
message ("         Fortran compiler ID    = ${CMAKE_Fortran_COMPILER_ID}")
if (CBT MATCHES "debug")
  message ("         Fortran flags          = ${CMAKE_Fortran_FLAGS_DEBUG}")
else ()
  set(CMAKE_Fortran_FLAGS ${CMAKE_Fortran_FLAGS_RELEASE})
  message ("         Fortran flags          = ${CMAKE_Fortran_FLAGS_RELEASE}")
endif ()
if(quad_precision)
  message ("         Precision              = Quad precision")
else()
  message ("         Precision              = Double precision")
endif ()
