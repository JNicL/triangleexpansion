!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                c0_m0m1m1.f90                                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!Last Modified: October 12, 2018

module c0_m0m1m1
  use triangle_aux, only: target_precision,prec,cone,cnul,choose
  implicit none
  ! for values z=m2/p2 > THRESHOLD expansion formulas are used
  real(prec), parameter :: C_0mm_exp_thr = real(2,kind=prec)

  contains

  function Crec(yk,yr,n)
    complex(prec), intent(in) :: yk,yr
    integer,       intent(in) :: n
    complex(prec)             :: Crec(n+1)
    integer                   :: i,l
    
    Crec(n+1) = yk**n * (cone-yk)**(n+1) / (yk-yr)**(n+1)
    do l = n, 1, -1
      Crec(l) = cnul
      do i = 1, n+1-l
        Crec(l) = Crec(l) + Crec(l+i) * lambda(yk,yr,i,n)
      end do
      Crec(l) = Crec(l) / (1+n-l)
      
    end do

    contains

    function lambda(yk,yr,i,n) result(lam)
      complex(prec), intent(in) :: yk,yr
      integer,       intent(in) :: i,n
      complex(prec)             :: lam

      lam = (1+n)*(cone/(yr-yk)**i - cone/(cone-yk)**i)-n/(-yk)**i
      
    end function lambda

  end function Crec


  function C0_n_m0m1m1(p2,m2,n) result(C0)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    complex(prec)             :: z0,z1,yr1,yr2,C0,loss,Cc1(n+1),Cc2(n+1)
    integer                   :: j

    z0 = m2(1)/p2
    z1 = m2(2)/p2

    yr1 = (cone + z0 - z1 - sqrt(4*z1 + (cone + z0 - z1)**2))/2
    yr2 = (cone + z0 - z1 + sqrt(4*z1 + (cone + z0 - z1)**2))/2

    Cc1 = Crec(yr1,yr2,n)
    Cc2 = Crec(yr2,yr1,n)

    C0 = log((yr1-cone)/yr1) * Cc1(1) + log((yr2-cone)/yr2) * Cc2(1)

    do j = 2, n+1
      C0 = C0 + ((-1)**(1+j) * yr1**(1-j) - (1-yr1)**(1-j))/(cone*(j-1)) * Cc1(j) &
              + ((-1)**(1+j) * yr2**(1-j) - (1-yr2)**(1-j))/(cone*(j-1)) * Cc2(j)
    end do

    C0 = C0 / (p2 * (1+n))

  end function C0_n_m0m1m1

end module c0_m0m1m1
