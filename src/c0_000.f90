!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_000.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! Implements the n-th derivative of C0(-p^2,-p^2(1+\delta),0,0,0)

module c0_000
  use triangle_aux, only: prec,cone,cnul,dir,muUV2,muIR2
  implicit none

  contains

  function C0_n_000(p2,m2,n) result(Cn)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    integer       :: k
    complex(prec) :: Cn,sum

    sum = cnul
    if (n .ge. 0) then
      do k = 0, n-1
        sum = sum + ( (-1)**k )/(cone+k)*(-1)**(n-k+1)/cmplx(n-k,kind=prec)
      end do
      sum = sum + 2*(-1)**n/(cone+n)*log(p2/muIR2)
    end if

    Cn = - sum/(2*p2)

    if (dir .ne. 0) then
      Cn = Cn + dir*C0_n_000_EP1(p2,m2,n)
    end if

  end function C0_n_000

  function C0_n_000_EP1(p2,m2,n) result(Cn)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    integer       :: k
    complex(prec) :: Cn

    Cn = -(-1)**(1+n)/(cone+n)/(p2)

  end function C0_n_000_EP1
    
end module c0_000
