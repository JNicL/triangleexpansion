!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 b0_m0m1.f90                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!Last Modified: October 21, 2018

module b0_m0m1
  use triangle_aux, only: target_precision,prec,cone,cnul,choose,pi,cima,duv,muUV2
  implicit none

  contains

  function Brec(yk,yr,n)
    complex(prec), intent(in) :: yk,yr
    integer,       intent(in) :: n
    complex(prec)             :: Brec(n+1)
    integer                   :: i,l
    
    Brec(n) = yk**n * (cone-yk)**(n) / (yk-yr)**(n)
    do l = n-1, 1, -1
      Brec(l) = cnul
      do i = 1, n-l
        Brec(l) = Brec(l) + Brec(l+i) * lambda(yk,yr,i,n)
      end do
      Brec(l) = Brec(l) / (n-l)
      
    end do

    contains

    function lambda(yk,yr,i,n) result(lam)
      complex(prec), intent(in) :: yk,yr
      integer,       intent(in) :: i,n
      complex(prec)             :: lam

      lam = n*(cone/(yr-yk)**i - cone/(cone-yk)**i-cone/(-yk)**i)
      
    end function lambda

  end function Brec

  function B0_0_m0m1(p2,m2) result(B0)
    complex(prec), intent(in) :: p2,m2(0:)
    complex(prec)             :: m0,m1,B0,z0,z1,xr1,xr2
    integer                   :: j

    m0 = sqrt(m2(0))
    m1 = sqrt(m2(1))
    z0 = m2(0)/p2
    z1 = m2(1)/p2

    xr1 = (cone + z0 - z1 - sqrt(4*z1 + (cone + z0 - z1)**2))/2
    xr2 = (cone + z0 - z1 + sqrt(4*z1 + (cone + z0 - z1)**2))/2

    !B0 = 2*cone - log(p2/muUV2) &
    !   + (xr1 - 1)*log(1 - xr1) + (xr2 - 1)*log(1 - xr2) &
    !   - xr1*log(-xr1) - xr2*log(-xr2) + pi*cima
    ! stable imaginary part 
    B0 = 2*cone - log(p2/muUV2) + duv &
       + (xr1 - 1)*log((1 - xr1)/(-xr1)) + (xr2 - 1)*log((1 - xr2)/(-xr2)) &
       - log(-xr1*xr2)


  end function B0_0_m0m1


  function B0_n_m0m1(p2,m2,n) result(B0)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    complex(prec)             :: z0,z1,xr1,xr2,B0,loss,Bc1(n),Bc2(n)
    integer                   :: j

    if (n .eq. 0) then
      B0 = B0_0_m0m1(p2,m2)
      return
    end if

    z0 = m2(1)/p2
    z1 = m2(2)/p2

    xr1 = (cone + z0 - z1 - sqrt(4*z1 + (cone + z0 - z1)**2))/2
    xr2 = (cone + z0 - z1 + sqrt(4*z1 + (cone + z0 - z1)**2))/2

    Bc1 = Brec(xr1,xr2,n)
    Bc2 = Brec(xr2,xr1,n)

    B0 = (-cone)**n + log((xr1-cone)/xr1) * Bc1(1) + log((xr2-cone)/xr2) * Bc2(1)

    do j = 2, n
      B0 = B0 + ((-1)**(1+j) * xr1**(1-j) - (1-xr1)**(1-j))/(cone*(j-1)) * Bc1(j) &
              + ((-1)**(1+j) * xr2**(1-j) - (1-xr2)**(1-j))/(cone*(j-1)) * Bc2(j)
    end do

    B0 = B0 / (n)

  end function B0_n_m0m1

end module b0_m0m1
