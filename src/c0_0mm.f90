!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_0mm.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! Implements the n-th derivative of C0(-p^2,-p^2(1+\delta),0,m^2,m^2)
! (C0_n_0mm_small_z). For large values of z = m^2/p^2 the formula is
! numerically unstable and an expansions in 1/z is performed (C0_n_exp_0mm_2,
! C0_n_exp_0mm). 

module c0_0mm
  use triangle_aux, only: target_precision,prec,cone,cnul,choose
  implicit none
  ! for values z=m2/p2 > THRESHOLD expansion formulas are used
  real(prec), parameter :: C_0mm_exp_thr = real(2,kind=prec)

  contains

  function C0_n_0mm(p2,m2,n) result(C0)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    complex(prec)             :: z,C0,loss

    z = m2(1)/p2
    if (abs(z) .gt. C_0mm_exp_thr) then
      C0 = C0_n_exp_0mm(z,p2,n,loss)
    else
      C0 = C0_n_0mm_small_z(z,p2,n)
    end if
    
  end function C0_n_0mm

  function C0_n_0mm_small_z(z,p2,n) result(Cn)
    ! Implements the closed formula which is stable for small z <= 2.
    ! z : m^2/p^2
    complex(prec), intent(in) :: z,p2
    integer,       intent(in) :: n
    complex(prec) :: Cn,sum
    integer       :: k

    if (n .lt. 0) then
      write (*,*) 'ERROR: called C0_n with n<0'
      stop
    end if

    sum = cnul
    if (n .gt. 0) then
      do k = 0, n-1
        sum = sum - (cone/(cone + z)**(n - k)/cmplx(k - n,kind=prec))
      end do
      sum = sum*(-1)**n
    end if

    Cn = (-(-1)**n*(log(cone + 1/z)) + sum)/(p2*(1+n))

  end function C0_n_0mm_small_z

  function C0_n_exp_0mm(z,p2,n,loss) result(Cn)
    complex(prec), intent(in) :: z,p2
    complex(prec), intent(out), optional :: loss
    integer,       intent(in) :: n
    complex(prec) :: w,Cn,Cnq
    integer       :: q

    if (n .lt. 0) then
      write (*,*) 'ERROR: called C0_n_exp_0mm with n<0'
      stop
    end if

    w = 1/z

    q = n+1
    Cn = C0_nq_coeff_0mm(w,n,q)
    Cnq = cnul
    if (present(loss)) then
      loss = Cn
    end if

    q = q+1
    Cnq = (-w)*(q-1)**2/cmplx(q,kind=prec)/cmplx(q-1-n,kind=prec)*Cn
    do while (abs(Cnq/Cn) .gt. target_precision)
      Cn = Cn + Cnq
      q = q + 1
      Cnq = (-w)*(q-1)**2/cmplx(q,kind=prec)/cmplx(q-1-n,kind=prec)*Cnq
    end do
    Cn = (Cn + Cnq)/cmplx(n+1,kind=prec)/p2

    if (present(loss)) then
     loss = log(abs(loss/Cn))/log(10._prec)
    end if

  end function C0_n_exp_0mm

  function C0_nq_coeff_0mm(w,n,q) result(C0)
    complex(prec), intent(in) :: w
    integer,       intent(in) :: n,q
    complex(prec) :: C0

    C0 = (-w)**cmplx(q,kind=prec)*cmplx(choose(q-1,n)/q,kind=prec)
    
  end function C0_nq_coeff_0mm

end module c0_0mm
