#!/bin/sh

for f in *.f90
do
 echo "Processing $f"
 #sed -e 's/prec/@T/g' -e 's/module \([^ ]+\)/module \1<T>/' $f > multiprec_sources/${f}t
 if [ $f == triangle_reduction.f90 ]
 then
   sed -e 's/prec)/@T)/g' -e 's/prec,/@T,/g' -e 's/\._prec/\._@T/g' -e 's/module \([^ ]*\)/module \1_<T>/g' -e 's/use \([^ |,]*\)\(,*\)/use \1_<T>\2/g' -e 's/function \([^ ]*\)(/function \1_<T>(/g' $f > multiprec_sources/${f}t
 else
   sed -e 's/\sprec)/\s@T)/g' -e 's/prec)/@T)/g' -e 's/prec,/@T,/g' -e 's/\._prec/\._@T/g' -e 's/\([0-9]\)_prec/\1_@T/g' -e 's/module \([^ ]*\)/module \1_<T>/g' -e 's/use \([^ |,]*\)\(,*\)/use \1_<T>\2/g' $f > multiprec_sources/${f}t
 fi
 #sed -e 's/function \([^ ]*_\)/module \1<T>/'
 #sed -e 's/module b0/module b0<T>/' $f > multiprec_sources/${f}t
 sed -i '1s;^;#definetype T DP dp\n#definetype T QP qp\n;' multiprec_sources/${f}t
 ./forpedo.py < multiprec_sources/${f}t > multiprec_sources/${f}
 rm multiprec_sources/${f}t
 # do something on $f
done
