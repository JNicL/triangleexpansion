!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 c0_m00.f90                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Last Modified: February 07, 2018

! Implements the n-th derivative of C0(-p^2,-p^2(1+\delta),m^2,0,0)
! (C0_n_m00_small_z). For large values of z = m^2/p^2 the formula is
! numerically unstable and a different representation in 1/z is chosen
! (C0_n_exp_m00). 

module c0_m00
  use triangle_aux, only: target_precision,prec,cone,cnul,Lphi,HarmNum,Sv1,LphiLog,dir,&
                          muIR2
  implicit none
  ! for values z=m2/p2 > THRESHOLD expansion formulas are used
  real(prec), parameter :: C_m00_exp_thr = 0.2_prec

  contains

  function C0_n_m00(p2,m2,n) result(C0)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    complex(prec)             :: z,C0

    z = m2(1)/p2
    if (abs(z) .gt. C_m00_exp_thr) then
      C0 = C0_n_m00_large_z(z,p2,n)
    else
      C0 = C0_n_m00_small_z(z,p2,n)
    end if

    if (dir .ne. 0) then
      C0 = C0 + dir*C0_n_m00_EP1(p2,m2,n)
    end if
    
  end function C0_n_m00

  function C0_n_m00_small_z(z,p2,n) result(Cn)
    complex(prec), intent(in) :: z,p2
    integer,       intent(in) :: n
    integer       :: k
    complex(prec) :: v,Cn,sum

    if (abs(z) .gt. 1) then
      write (*,*) 'ERROR: C0_n_m00_small_z not convergent for |z|>1'
      stop
    end if

    v = 1/(1+z)

    sum = cnul
    if (n .gt. 0) then
      do k = 0, n
        sum = sum + ((cone + z)**(n - k)-2)/cmplx(1+ k,kind=prec)
      end do
      sum = sum + 1/cmplx(1+ n,kind=prec)
    end if

    Cn = (-v)**(n+1)*(sum + log(z*v)*Sv1(z,n+1) - log(v) - log(muIR2/p2) &
                     )/ cmplx(n+1,kind=prec)/p2

  end function C0_n_m00_small_z

  function C0_n_m00_large_z(z,p2,n) result(Cn)
    complex(prec), intent(in) :: z,p2
    integer,       intent(in) :: n
    complex(prec) :: w,v,Cn

    if (n .lt. 0) then
      write (*,*) 'ERROR: called C0_n_m00_large_z with n<0'
      stop
    end if

    w = 1/z
    v = w/(cone+w)

    Cn = -(-v)**(n+1)*(v*Lphi(v,n+2) + 2*HarmNum(n)+cone/cmplx(n+1,kind=prec) &
                       + log(w) - 2*log(1+w)+log(muIR2/p2)                 &
                      )/cmplx(n+1,kind=prec)/p2

  end function C0_n_m00_large_z

  function C0_n_m00_EP1(p2,m2,n) result(Cn)
    complex(prec), intent(in) :: p2,m2(:)
    integer,       intent(in) :: n
    integer       :: k
    complex(prec) :: Cn,w

    w = 1/(1+m2(1)/p2)
    Cn = -(-w)**(n+1)/(cone+n)/(p2)

  end function C0_n_m00_EP1
    
end module c0_m00
