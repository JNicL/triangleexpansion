TrRed is part of trred & OpenLoops2
Copyright (C) 2017-2018 Federico Buccioni, Jean-Nicolas Lang,
                        Stefano Pozzorini, Hantian Zhang and Max Zoller

TrRed has been developed by Jean-Nicolas Lang, Hantian Zhang and Federico Buccioni
TrRed is licenced under the GNU GPL version 3, see COPYING for details.

TrRed 
=====

* Version 0.9.3

Numerical evaluation for triangle configurations: 

                 -->                      <-- 
                 p1 _____________________ -p2
                       \             /
                        \           /
     p1^2= -p^2          \         /
                          \       /
     p2^2= -p^2(1+dlt)     \     /
                            \   /
                             \ /
                              |
                              |
                           (p2-p1)^2 = 0


Compilation and running tests
-----------------------------

Run:

    cd build
    cmake ..
    make
    make check

### Contact ###

  - Jean-Nicolas Lang <jlang@physik.uzh.ch>
  - Hantian Zhang <hantian.zhang@physik.uzh.ch>
  - Federico Buccioni <buccioni@physik.uzh.ch>
