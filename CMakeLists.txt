################################################################################
#                       trianglereduction configuration                        #
################################################################################
# File: CMakeLists.txt
# Author: Jean-Nicolas Lang
# Description: CMake configure file for tr
# Last Modified: December 27, 2017

set(CMAKE_MACOSX_RPATH 1) 
set(CMAKE_CACHEFILE_DIR  CMakeFiles/)
cmake_minimum_required (VERSION 2.8.12)

#-----------------------#
#  Project declaration  #
#-----------------------#

project (trianglereduction Fortran C CXX)

option(quad_precision "Compile the library in quad precision, instead of double precision" Off)
option(collier_demos "Compile checks against collier" Off)


set(TR_MAJOR_VERSION 0)
set(TR_MINOR_VERSION 1)
set(TR_PATCH_VERSION 0)
set(TR_VERSION ${TR_MAJOR_VERSION}.${TR_MINOR_VERSION}.${TR_PATCH_VERSION})

set(TR_ROOT_PATH ${PROJECT_SOURCE_DIR})
set(TR_SOURCE_PATH ${TR_ROOT_PATH}/src)
set(TR_INCLUDE_PATH $TR_ROOT_PATH}/include)

# Set modules directory
set(LIB_LOCAL_DIR ${PROJECT_SOURCE_DIR})
set(SYSCONFIG_LOCAL_DIR ${LIB_LOCAL_DIR}/config)
set(CMAKE_Fortran_MODULE_DIRECTORY ${LIB_LOCAL_DIR}/include)
set(INCLUDE_LOCAL_DIR ${LIB_LOCAL_DIR}/include)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${LIB_LOCAL_DIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${LIB_LOCAL_DIR})
include_directories (${CMAKE_Fortran_MODULE_DIRECTORY})

# add sources
add_subdirectory(src)

# add demos files using collier
if (collier_demos)
  add_subdirectory(collier_checks)
endif()

#---------#
#  Tests  #
#---------#

enable_testing()
add_subdirectory(tests)
